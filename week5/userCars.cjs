/* eslint-disable no-unused-vars */
require("dotenv").config();

/** 
 * Connect to MongoDB
 */
const mongoose = require("mongoose");

mongoose.connect(
    "mongodb://localhost/exercises",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        user: process.env.MONGODB_USER,
        pass: process.env.MONGODB_PASS
    }
)
    .catch(err => console.log(err));

/** 
 * Schema  
 * */
const UserCarSchema = new mongoose.Schema({
    owner_firstname: String,
    owner_surname: String,
    contact_email: String,
    owner_address: String,
    owner_state: String,
    car_make: String,
    car_model: String,
    car_modelyear: Number,
    price: Number
});

/** 
 * Model 
 * */
const UserCar = mongoose.model("user_cars", UserCarSchema);

/** 
 * JSON Data to insert to database 
 * */
const userCars = require("./user_cars.json");
importJSONData(userCars, UserCar);

/** 
 * Helper functions
 */
function importJSONData(jsonData, Model) {
    Model.insertMany(jsonData)
        .then(() => {
            console.log("Success");
        })
        .catch(err => {
            console.log(err);
        });
}

// async function connectMongoose() {
//     await mongoose.connect(
//         "mongodb://localhost/exercises",
//         {
//             useNewUrlParser: true,
//             useUnifiedTopology: true,
//             user: process.env.MONGODB_USER,
//             pass: process.env.MONGODB_PASS
//         }
//     );
// }