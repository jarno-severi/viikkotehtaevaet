import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    accountBalance: { type: Number, required: true }
});

export const User = mongoose.model("User", UserSchema, "bank");