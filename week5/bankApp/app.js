import express from "express";
import bankRouter from "./api/bank.router.js";
import { connectDatabase } from "./controllers/db.controller.js";

const app = express();
const PORT = 5000;

connectDatabase();
app.use(express.json());
app.use("/bank", bankRouter);

app.listen(PORT, () => console.log(`Listening at PORT: ${PORT}`));