import express from "express";
import * as usersController from "../controllers/users.controller.js";

const router = express.Router();

router.post("/user", usersController.createUser);
router.post("/transfer", usersController.transferMoney);
router.get("/:id/balance", usersController.getBalance);
router.patch("/user/deposit", usersController.depositMoney);
router.patch("/user/password", usersController.updatePassword);
router.patch("/update", usersController.updateUser);
router.patch("/user/withdraw", usersController.withdrawMoney);

export default router;