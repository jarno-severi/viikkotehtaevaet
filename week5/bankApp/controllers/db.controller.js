import mongoose from "mongoose";
import "dotenv/config";

export const connectDatabase = function () {
    const uri = process.env.DATABASE_URI;
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        user: process.env.MONGODB_USER,
        pass: process.env.MONGODB_PASS
    };
    mongoose.connect(uri, options)
        .catch(error => console.log(error));
};