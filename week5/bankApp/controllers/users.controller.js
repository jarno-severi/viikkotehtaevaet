/* eslint-disable no-unused-vars */
import bcrypt from "bcrypt";
import { User } from "../models/users.model.js";

/**
 * POST
 * @param {object} req -usrename, initial deposit, password 
 * @return user_id
 */
export const createUser = async (req, res) => {
    const password = await bcrypt.hash(req.body.password, 10);
    const user = new User({
        username: req.body.username,
        password: password,
        accountBalance: req.body.deposit
    });
    user.save()
        .then(result => res.json(result._id))
        .catch(err => res.status(400).send(err._message));
};

/**
 * POST
 * @param {object} req -id, password, recipient_id, amount
 * @return new_account_balance
 */
export const transferMoney = async (req, res) => {
    try {
        const user = await User.findById(req.body.id);
        const recipient = await User.findById(req.body.recipient);
        const correctPassword = await bcrypt.compare(req.body.password, user.password);

        if (correctPassword) {
            const balance = user.accountBalance - req.body.amount;

            if (balance > 0) {
                user.accountBalance = user.accountBalance - req.body.amount;
                recipient.accountBalance = recipient.accountBalance + req.body.amount;
                await recipient.save();
                const result = await user.save();
                res.json(result.accountBalance.toFixed(2));
            } else {
                res.status(400).send(`Not enough balance to tranfer ${req.body.withdraw.toFixed(2)}`);
            }

        } else {
            res.status(401).send("Invalid password");
        }

    } catch (err) {
        res.status(404).send("Account not found");
    }
};

/**
 * GET
 * @param {object} req -id
 * @return account_balance
 */
export const getBalance = async (req, res) => {
    try {
        const result = await User.findById(req.params.id);
        res.json(result.accountBalance.toFixed(2));
    } catch (err) {
        res.status(404).send("User not found");
    }
};

/**
 * PATCH
 * @param {object} req -id, password, amount
 * @return new_account_balance
 */
export const depositMoney = async (req, res) => {
    try {
        const user = await User.findById(req.body.id);
        const correctPassword = await bcrypt.compare(req.body.password, user.password);

        if (correctPassword) {
            user.accountBalance = user.accountBalance + req.body.deposit;
            const result = await user.save();
            res.json(result.accountBalance.toFixed(2));
        } else {
            res.status(401).send("Invalid password");
        }

    } catch (err) {
        res.status(404).send("User not found");
    }
};

/**
 * PATCH
 * @param {object} req -id, new_password
 * @return new_password
 */
export const updatePassword = async (req, res) => {
    try {
        const user = await User.findById(req.body.id);
        const newPassword = await bcrypt.hash(req.body.password, 10);
        user.password = newPassword;
        const result = await user.save();
        res.json(result.password);
    } catch (err) {
        res.status(404).send("User not found");
    }
};

/**
 * PATCH
 * @param {object} req -id, new_username
 * @return new_username
 */
export const updateUser = async (req, res) => {
    try {
        const user = await User.findById(req.body.id);
        user.username = req.body.username;
        const result = await user.save();
        res.json(result.username);
    } catch (err) {
        res.status(404).send("User not found");
    }
};

/**
 * PATCH
 * @param {object} req -id, password, amount 
 * @return user_id
 */
export const withdrawMoney = async (req, res) => {
    try {
        const user = await User.findById(req.body.id);
        const correctPassword = await bcrypt.compare(req.body.password, user.password);

        if (correctPassword) {
            const balance = user.accountBalance - req.body.withdraw;

            if (balance > 0) {
                user.accountBalance = user.accountBalance - req.body.withdraw;
                const result = await user.save();
                res.json(result.accountBalance.toFixed(2));
            } else {
                res.status(400).send(`Not enough balance to withdraw ${req.body.withdraw.toFixed(2)}`);
            }

        } else {
            res.status(401).send("Invalid password");
        }

    } catch (err) {
        res.status(404).send("Account not found");
    }
};