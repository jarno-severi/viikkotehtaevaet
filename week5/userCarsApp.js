/* eslint-disable no-unused-vars */
import express from "express";
import mongoose from "mongoose";
import "dotenv/config";
import { UserCar } from "./models/userCars.model.js";

const app = express();
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Listening to ${PORT}.`));

/** Connect to local exercises database */
connectDatabase("mongodb://localhost/exercises");

/**
 * Middleware:
 * JSON: parses incoming JSON requests and puts the parsed data in req.body
 */
app.use(express.json());

/**
 * Routes
 */
app.get("/userCar/:id", getCarById);
app.post("/userCar", postUserCar);

/**
 * Connect to MongoDB database
 * @param {string} uri -mongodb://localhost/databasename
 */
function connectDatabase(uri) {
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        user: process.env.MONGODB_USER,
        pass: process.env.MONGODB_PASS
    };
    mongoose.connect(uri, options)
        .catch(error => console.log(error));
}

/**
 * Get user car by id from database
 * @param {object} req -HTTP Request
 * @param {object} res -JSON object
 */
function getCarById(req, res) {
    UserCar.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => console.log(err));
}

/**
 * Post user car to database
 * @param {object} req -HTTP Request
 * @param {object} res -JSON object
 */
function postUserCar(req, res) {
    const document = new UserCar(req.body);
    document.save()
        .then((result) => res.json(result._id))
        .catch(err => console.log(err));
}