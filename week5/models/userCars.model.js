import mongoose from "mongoose";

const UserCarSchema = new mongoose.Schema({
    owner_firstname: String,
    owner_surname: String,
    contact_email: String,
    owner_address: String,
    owner_state: String,
    car_make: String,
    car_model: String,
    car_modelyear: Number,
    price: Number
});

export const UserCar = mongoose.model("user_cars", UserCarSchema);