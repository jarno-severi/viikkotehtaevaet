const input = process.argv[2];

function reverseWord(word) {

    if (word.length === 1) return word;

    const arr = word.split("");
    const reverseArr = [];

    for (let i = (arr.length - 1); i >= 0; i--) {
        reverseArr.push(arr[i]);
    }

    return reverseArr.join("");
}

// console.log(reverseWord(input));

if (input === reverseWord(input)) console.log(`Yes, '${input}' is a palindrome`);
else console.log(`No, '${input}' is not a palindrome`);