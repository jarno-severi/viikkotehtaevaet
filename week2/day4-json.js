import fs from "fs";

/* const forecast = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};

const saveForecastsToFile = () => {
    fs.writeFile("forecast_data.json", JSON.stringify(forecast), "utf8", (err) => {
        if (err) {
            console.log("Could not save forecasts to file!");
        }
    });
};

saveForecastsToFile(); */



const readForecastsFromFile = (file) => {
    try {
        const data = fs.readFileSync(file, "utf8");
        const json = JSON.parse(data);
        console.log(`original tempererature: ${json.temperature}`);
        json.temperature = 20;
        return json;
    } catch (e) {
        console.log("No saved data found.");
    }
};

const saveForecastsToFile = (data) => {
    fs.writeFile("forecast_data.json", JSON.stringify(data), "utf8", (err) => {
        if (err) {
            console.log("Could not save forecasts to file!");
        }
    });
};

const newForecast = readForecastsFromFile("forecast_data.json"); // Read from file
saveForecastsToFile(newForecast); // Write to file