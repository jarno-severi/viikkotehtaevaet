/* // Exer. 1
const arg = process.argv[2];

function firstLetterToUpperCase(arg) {
    const splitArg = arg.split(" ");
    const upperCaseLetters = splitArg.map(el => el[0].toUpperCase() + el.substring(1));
    
    console.log(upperCaseLetters);

    // for (let i = 0; i < splitArg.length; i++) {
    //     let firstLetter = splitArg[i][0];
    //     let letterToUpperCase = firstLetter.toUpperCase();
    //     let newString = letterToUpperCase + splitArg[i].substring(1);
    //     splitArg[i] = newString;
    // }

    // console.log(splitArg.join(" "));
}

firstLetterToUpperCase(arg); */


/* // Exer. 2
function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

// const getRandom = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

console.log(getRandom(1, 6)); */


/* // Exer. 3: Write a program that contains a function, which calculates the factorial n of a given number n
function getFactorial(n) {
    if (n === 1) return n;
    return n * getFactorial(n - 1);
}

console.log(getFactorial(4));
 */

/* // Jotain
const instruments = ["Accordion", "Piano", "Synth", "Nyckelharpa"];

const instrumentLength = (instrument) => instrument.length; // Helper function that gets called

const lengths = instruments.map(instrumentLength);
console.log(lengths); */

/* // Exer. 4
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
// divisible by three
const divisibleByThree = arr.filter(number => number % 3 === 0);
console.log(divisibleByThree);
// multiplied by 2
const multiplyByTwo = arr.map(number => number * 2);
console.log(multiplyByTwo);
// summ all of the values
const sumAll = arr.reduce((acc, number) => acc + number);
console.log(sumAll); */

/* // Exer. 5: find the first non-repeating character from a string
const string = "waabbooooojfffkkccjdddTTT";

// BUUTTI RATKAISU:
let firstUnique = string.split("").find((s, i) => s !== string[i - 1] && s !== string[i + 1]);
console.log(firstUnique);
 */

/* // Exer. 6: Create a program that joins two arrays together. If the same value appears in both arrays, the function should return that value only once.
const arr1 = [1, 2, 3, 4];
const arr2 = [3, 4, 5, 6];
const arr3 = [];

for(let i = 0; i < arr1.length; i++) {
    if (arr2.find(number => number === arr1[i])) console.log("same number not");
    else arr3.push(arr1[i]);
}

const mergedArr = arr3.concat(arr2);
console.log(mergedArr);

// // BUUTTI RATKAISU: 
// const arr = arr1.concat(arr2);
// const arrDuplicatesRemoved = arr.filter((n, i) => !arr.slice(i + 1).includes(n));  */

/* // Exer. 7
function randomNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    const random = Math.floor(Math.random() * (max - min + 1) + min);
    return random;
}

const lotteryNumbers = [];

while (lotteryNumbers.length < 7) {
    let number = randomNumber(1, 40);
    if (!lotteryNumbers.includes(number)) {
        lotteryNumbers.push(number);
    }
}

// console.log(lotteryNumbers);

let i = 0;
const lottery = setInterval(() => {
    if (i > 5) clearInterval(lottery);
    console.log(lotteryNumbers[i]);
    i++;
}, 1000); */

/* // Exer. 8
function fibonacci(n) {
    let number1 = 0;
    let number2 = 1;
    let next = 1;
    const arr = [];

    for(let i = 1; i <= n; i++) {
        arr.push(next);
        next = number1 + number2;
        number1 = number2;
        number2 = next;
    }

    console.log(arr);
}

fibonacci(20); */

/* // BUUTTI Functio-ratkaisu:
function fibonacci(a,b,i,arr) {
    arr.push(a + b);
    if(i >= 20)
        return;
    
    fibonacci(b, a+b, i+1, arr);
}

const arr = [1,1];
fibonacci(1,1,0,arr);

console.log(arr); */