/* Create a programs that reverses each word in a string. */

const input = process.argv[2].split(" ");

function reverseWord(word) {

    if (word.length === 1) return word;

    const arr = word.split("");
    const reverseArr = [];

    for (let i = (arr.length - 1); i >= 0; i--) {
        reverseArr.push(arr[i]);
    }

    return reverseArr.join("");
}

const reverseInput = input.map(reverseWord);

console.log(reverseInput.join(" "));