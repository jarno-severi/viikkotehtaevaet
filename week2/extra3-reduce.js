/* // 1: Turn an array of numbers into a total of all the numbers

function total(arr) {
    return arr.reduce((prev, current) => prev + current);
}

console.log(total([1, 2, 3])); // 6

// 2: Turn an array of numbers into a long string of all those numbers.
function stringConcat(arr) {
    return arr.reduce((prev, current) => prev.toString() + current.toString());
}

console.log(stringConcat([1, 2, 3])); // "123" */

/* // 3: Turn an array of voter objects into a count of how many people voted
const voters = [
    { name: "Bob", age: 30, voted: true },
    { name: "Jake", age: 32, voted: true },
    { name: "Kate", age: 25, voted: false },
    { name: "Sam", age: 20, voted: false },
    { name: "Phil", age: 21, voted: true },
    { name: "Ed", age: 55, voted: true },
    { name: "Tami", age: 54, voted: true },
    { name: "Mary", age: 31, voted: false },
    { name: "Becky", age: 43, voted: false },
    { name: "Joey", age: 41, voted: true },
    { name: "Jeff", age: 30, voted: true },
    { name: "Zack", age: 19, voted: false }
];

function totalVotes(arr) {
    const voted = arr.reduce((acc, current) => {
        if (current.voted) acc + 1;
        return acc;
    }, 0);

    return voted;
}

console.log(totalVotes(voters)); // 7 */

/* // 4. Given an array of all your wishlist items, figure out how much it would cost to just buy everything at once
const wishlist = [
    { title: "Tesla Model S", price: 90000 },
    { title: "4 carat diamond ring", price: 45000 },
    { title: "Fancy hacky Sack", price: 5 },
    { title: "Gold fidgit spinner", price: 2000 },
    { title: "A second Tesla Model S", price: 90000 }
];

function shoppingSpree(arr) {
    return arr.reduce((previous, current) => previous + current.price, 0);
}

console.log(shoppingSpree(wishlist)); // 227005 */

/* // 5. Given an array of arrays, flatten them into a single array
function flatten(arr) {
    return arr.reduce((prev, current) => prev.concat(current)); // prev.toString() + current.toString());
}

const arrays = [
    ["1", "2", "3"],
    [true],
    [4, 5, 6]
];

console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6]; */

// Given an array of potential voters, return an object representing the results of the vote


/* // Ramin oma reduce funktio, jossa callbackFn vastaa funktiota, joka reducelle annetaan lambda-lausekkeena
const array = [1,3,5,7,9];

function reduce(callbackFn, initialValue) {
    let accumulator = initialValue;
    for (let i = 0; i < array.length; i++) {
        accumulator = callbackFn(accumulator, array[i]);
    }
    return accumulator;
} */


/* TÄSSÄ MENOSSA */


// Given an array of potential voters, return an object representing the results of the vote: 
// Include how many of the potential voters were in the ages 18-25, how many from 26-35, how many from 36-55, 
// and how many of each of those age ranges actually voted. The resulting object containing this data should have 6 properties. 
// See the example output at the bottom.

const voters = [
    { name: "Bob", age: 30, voted: true },
    { name: "Jake", age: 32, voted: true },
    { name: "Kate", age: 25, voted: false },
    { name: "Sam", age: 20, voted: false },
    { name: "Phil", age: 21, voted: true },
    { name: "Ed", age: 55, voted: true },
    { name: "Tami", age: 54, voted: true },
    { name: "Mary", age: 31, voted: false },
    { name: "Becky", age: 43, voted: false },
    { name: "Joey", age: 41, voted: true },
    { name: "Jeff", age: 30, voted: true },
    { name: "Zack", age: 19, voted: false }
];

function voterResults(arr) {

    return arr.reduce((obj, voter) => {
        if (voter.age > 17 && voter.age < 26) {
            obj.numYoungPeople += 1;
            if (voter.voted) obj.numYoungVotes += 1;
        }
        if (voter.age > 25 && voter.age < 36) {
            obj.numMidsPeople += 1;
            if (voter.voted) obj.numMidVotesPeople += 1;
        }
        if (voter.age > 35 && voter.age < 56) {
            obj.numOldsPeople += 1;
            if (voter.voted) obj.numOldVotesPeople += 1;
        }
        return obj;

    }, { numYoungVotes: 0, numYoungPeople: 0, numMidVotesPeople: 0, numMidsPeople: 0, numOldVotesPeople: 0, numOldsPeople: 0 });
}

console.log(voterResults(voters)); // Returned value shown below: