/*  Write a function that takes a string and returns an object representing the character count for each
letter. Use .reduce to build this object. ex. */

const countLetters = function (string) {
    const alphabets = [ "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" ];
    const input = string.split("");
    const obj = {};

    alphabets.forEach(alphabet => {
        const filtered = input.filter(element => element === alphabet);
        if (filtered.length > 0) obj[alphabet] = filtered.length;
    });

    return obj;
};

console.log(countLetters("abbcccddddeeeee")); // => {a:1, b:2, c:3, d:4, e:5}
console.log(countLetters("aaergergerreharkemhkapgrkorgaeopgreo"));

/* function increaseObjectAttributeCount(object, attribute){
    if (attribute in object) object[attribute] += 1;
    else object[attribute] = 1;
}
const countLetters = function(string){
    const array = string.split("");
    return array.reduce((obj, char) => {
        increaseObjectAttributeCount(obj, char);
        return obj;
    }, {});
}; */