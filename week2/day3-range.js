/* Write a program that takes in any two numbers from the command line, start and end. 
The program creates and prints an array filled with numbers from start to end .

Examples:
node .\createRange.js 1 5 -> [1, 2, 3, 4, 5]
node .\createRange.js -5 -1 -> [-5, -4, -3, -2, -1]
node .\createRange.js 9 5 -> [9, 8, 7, 6, 5]

Note the order of the values. When start is smaller than end , the order is ascending and when start is greater than end , order is descending. */

const input = [parseInt(process.argv[2]), parseInt(process.argv[3])];

function createArray(start, end) {
    const array = [];

    if (start > end) {
        const arrayLength = (start - end) + 1;
        let number = start;

        for (let i = 0; i < arrayLength; i++) {
            array.push(number);
            number -= 1;
        }
    }

    else if (start < end) {
        const arrayLength = (end - start) + 1;
        let number = start;

        for (let i = 0; i < arrayLength; i++) {
            array.push(number);
            number += 1;
        }
    }

    else console.log("ERROR");

    return array;
}

console.log(createArray(input[0], input[1]));