// Create a function that takes in an operator and two numbers and returns the result.

function calculator(operator, num1, num2) {
    switch (operator) {
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "*":
            return num1 * num2;
        case "/":
            return num1 / num2;
        default:
            return "Can't do that!";
    }
}

console.log(calculator("+", 2, 3));
console.log(calculator("-", 2, 3));
console.log(calculator("*", 2, 3));
console.log(calculator("/", 2, 3));
console.log(calculator("p", 2, 3));