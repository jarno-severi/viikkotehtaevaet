class Shape {
    constructor(width, heigth) {
        this.width = width;
        this.heigth = heigth;
    }

    calculateArea() {
        return -1;
    }

    calculateCirc() {
        return -1;
    }
}

class Circle extends Shape {
    constructor(width, heigth) {
        super(width, heigth);
        this.radius = width / 2;
    }

    calculateCirc() {
        return 2 * Math.PI * this.radius;
    }

    calculateArea() {
        return Math.PI * this.radius ** 2;
    }

}

class Rectangle extends Shape {
    constructor(width, heigth) {
        super(width, heigth);
    }

    calculateArea() {
        return this.width * this.heigth;
    }

    calculateCirc() {
        return this.width * 2 + this.heigth * 2;
    }
}

// class Triangle extends Shape {

// }

const box1 = new Rectangle(2,3);
const circle1 = new Circle(5);

const shapes = [box1, circle1];
shapes.forEach(shape => {
    console.log(`Area: ${shape.calculateArea()}`);
    console.log(`Circle: ${shape.calculateCirc()}`);
    console.log("---------------------------");
});