/* function Student(className, mathGrade, finnishGrade, historyGrade) { // constructor function
    this.className = className;
    this.mathGrade = mathGrade;
    this.finnishGrade = finnishGrade;
    this.historyGrade = historyGrade;
}

Student.prototype.calculateAvarage = function () {
    return (this.mathGrade + this.finnishGrade + this.historyGrade) / 3;
};

Student.prototype.finishYear = function () {
    let year = parseInt(this.className[0]);
    if (year === 9) return "Graduated";
    else {
        year += 1;
        return this.className = year.toString() + this.className[1];
    }
}; */

class Student{
    constructor(className, mathGrade, finnishGrade, historyGrade) {
        this.className = className;
        this.mathGrade = mathGrade;
        this.finnishGrade = finnishGrade;
        this.historyGrade = historyGrade;
    }

    calculateAvarage() {
        return (this.mathGrade + this.finnishGrade + this.historyGrade) / 3;
    }

    finishYear() {
        let year = parseInt(this.className[0]);
        if (year === 9) {
            return "Graduated";
        } else {
            year += 1;
            return this.className = year.toString() + this.className[1];
        }
    }
}

const peetu = new Student("7A", 7, 6, 7);

console.log(peetu.className);
console.log(peetu.calculateAvarage());
console.log(peetu.finishYear());