// Create a function that takes in an array and returns a new array of values that are above average.

function aboveAverage(arr) {
    const sum = arr.reduce((prev, current) => prev + current);
    const avarage = sum / arr.length;
    return arr.filter(value => value > avarage);
}

console.log(aboveAverage([1, 5, 9, 3]));  // outputs an array that has values greater than 4.5c