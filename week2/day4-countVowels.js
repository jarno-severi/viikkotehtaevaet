// Return the number (count) of vowels in the given string.
function getVowelCount(string) {
    const vowels = ["a", "e", "i", "o", "u", "y"];
    const arr = string.split("");
    const filteredArr = arr.filter(element => vowels.includes(element));
    return filteredArr.length;
}

console.log(getVowelCount("abracadabra")); // 5
console.log(getVowelCount("aegegaggarkmgawrkpwekpoewopgpweog"));