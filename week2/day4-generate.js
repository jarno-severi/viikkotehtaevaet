/* Create a function (or multiple functions) that generates username and password from given firstname and lastname.
Username: B + last 2 numbers from current year + 2 first letters from both last name and first name in lower case
Password: 1 random letter + first letter of first name in lowercase + last letter of last name in uppercase + 
random special character + last 2 numbers from current year */

function generateCredentials(first, last) {
    // username
    const year = new Date().getFullYear().toString();
    const firstTwoFromYear = year.slice(0, 2);
    const firstNameLetters = first.slice(0, 2).toLowerCase();
    const lastNameLetters = last.slice(0, 2).toLowerCase();
    const userName = `B${firstTwoFromYear}${lastNameLetters}${firstNameLetters}`;

    // password
    const randomL = getRandom(65, 90); // ASCII table range for uppercase letters
    const randomC = getRandom(33, 47); // ASCII table range for special characters
    const randomLetter = String.fromCharCode(randomL);
    const randomChar = String.fromCharCode(randomC);
    const lastLetter = last[last.length - 1].toUpperCase();
    const lastTwoFromYear = year.slice(2, 4);
    const password = `${randomLetter}${firstNameLetters[0]}${lastLetter}${randomChar}${lastTwoFromYear}`;

    return { userName, password };
}

function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

console.log(generateCredentials("John", "Doe")); // John Doe -> B20dojo, mjE(20