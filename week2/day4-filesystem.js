import fs from "fs";

// fs.readFile
fs.readFile("./textFile.txt", "utf-8", (err, data) => {
    if (err) console.log(err);
    else {
        const rows = data.split("\n");
        const newRows = [];

        rows.forEach(row => {
            const arr = row.split(" ");
            const lowCaseArr = arr.map(word => word.toLowerCase());
            const newArr = lowCaseArr.map(word => {
                if (word === "joulu") return word = "kinkku";
                if (word === "lapsilla") return word = "poroilla";
                return word;
            });
            const newRow = newArr.join(" ");
            newRows.push(newRow);
        });

        const newData = newRows.join("\n");

        // write this new data to new file
        fs.writeFile("./newText.txt", newData, (err) => {
            if (err) console.log(err);
            else console.log("success");
        });
    } 
});


// fs.createWriteStream

// fs.createReadSream