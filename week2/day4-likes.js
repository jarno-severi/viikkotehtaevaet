/* Create a "like" function that takes in a array of names. 
Depending on a number of names (or length of the array) the function must return strings as follows:

For 4 or more names, "2 others" simply increases */

function like(arr) {

    if (arr.length === 0) return "no one likes this :(";

    if (arr.length > 3) {
        const firstTwo = arr.slice(0, 2).join(", ");
        const lengthMinusTwo = arr.length - 2;
        return `${firstTwo} and ${lengthMinusTwo} others likes this!`;

    } else if (arr.length === 2) {
        return arr.join(" and ") + " like this!";

    } else return arr.join(", ") + " like this!";
}

console.log(like([]));
console.log(like(["John"])); // "John likes this"
console.log(like(["Mary", "Alex"])); // "Mary and Alex like this"
console.log(like(["John", "James", "Linda"])); // "John, James and Linda like this"
console.log(like(["Alex", "Linda", "Mark", "Max"])); // must be "Alex, Linda and 2 others
console.log(like(["Alex", "Linda", "Mark", "Max", "Tom", "Cat"])); // must be "Alex, Linda and 2 others