/* Create a program that every time you run it, prints out an array with differently randomized order of the array above.
Example: node .\randomizeArray.js -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10] */

const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

const shuffleArray = (array) => {
    for (let i = array.length - 1; i > 0; i--) { // i = 9, 8, 7, 6, 5, 4, 3, 2, 1, so loop for 9 times
        const random = Math.floor(Math.random() * (i + 1)); // random between 0-9, 0-8, 0-7, 0-6, 0-5....
        const temp = array[i]; // temp = 32, random[0-9], random[0-8], random[0-7]...
        array[i] = array[random]; // arr[8] = arr[random between 0-8] eli yksi jonon numroista tallennetaan jonon viimeiseksi pois tulevilta kierroksilta, kunnes kaikki numerot on käyty läpi
        array[random] = temp; // 32, random[0-9], random[0-8], random[0-7]....
    }
    return array;
};
  

console.log(shuffleArray(array));