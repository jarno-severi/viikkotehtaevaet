/* const anotherFunction = function (arg, func) {
    // arg menee callback funktiolle parametrina
    return func(arg); // palauttaa callbackin, jolla on oma return
}

// function callback(arg) { // tänne tulee anotherFunctionin argumenteista 2
//     console.log(`lasketaan ${arg} * 5` );
//     return arg * 5; // palauttaa 2 * 5 resultiin, jonka anotherFunction palauttaa lopullisena arvona
// }

console.log(anotherFunction(2, value => value * 5)); // return 2 * 5 = 10
// lambda-funktio on anotherFunction func parametri

// koska anotherFunction palauttaa sille syötetyn func (callback), joka taasen tässä tapauksessa palauttaa arvon 10
// tämä func (callback) ottaa argumenttina value, joka on tässä tapauksessa 2, joka tulee anotherFunctioniin syötetystä arg parametrista */

countdown(5);

function countdown (number = 3) {
    setTimeout(() => {

        if (number === 0) {
            console.log("LAUNCH!");
            return;
        }
        
        console.log(number);
        countdown(number - 1);
    }, 1000);
}

