const getValue = function () {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

async function results() {
    try {
        const first = await getValue();
        const second = await getValue();
        console.log(`Value 1 ${first.value} and value 2 ${second.value}`);
        
    } catch (err) {
        console.log(err);
    }
}

results();

/* // PROMISE ALL 
Promise.all([getValue(), getValue()])
.then(values => {
    console.log(Value 1 is ${values[0].value} and value 2 is ${values[1].value});
}); */