/* import fs from "fs";

function getPassword(file) {
    return new Promise((resolve, reject) => {
        const txt = fs.readFileSync(file, "utf-8");

        if (txt === "S4l4s4n4!!") {
            resolve("Welcome user!");
        } else {
            reject("Wrong password!");
        }
    });
}

getPassword("test.txt")
    .then(msg => {
        console.log(msg);
        // lataa salaiset dokumentit
        // -----
    })
    .catch(err => {
        console.log(err);
    }) */


function print(txt) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log(txt);
            resolve();
        }, 1000);
    });
}

print(3)
    .then(() => print(2))
    .then(() => print(1))
    .then(() => print("GO"));
    //.catch(err) => error

/* async function countdown() {
    try {
        await print(3);
        await print(2);
        await print(1);
        await print("GO");
    } catch (err) {
        error
    }
}

countdown(); */