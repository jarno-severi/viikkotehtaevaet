import express from "express";
import fs from "fs";

const app = express();
let counter = 1;

app.use("/counter", (req, res, next) => {
    const timeStamp = new Date();
    let data = "";

    if (req.query.number) {
        data = `${timeStamp}: Counter called in: ${req.path} with query number=${req.query.number}\r\n`; 
        // req.query voidaan muuttaa tekstiksi JSON.stringify
    } else {
        data = `${timeStamp}: Counter called in: ${req.path} with no query\r\n`;
    }

    fs.appendFile("log.txt", data, err => {
        if (err) console.log(err);
        else next();
    });
});

app.get("/counter", (req, res) => {
    const reqNumber = parseInt(req.query.number);

    if (reqNumber) counter = reqNumber;

    res.send(`<h1>${counter}</h1>`);
    counter++;
});

const counterWithName = {};
app.get("/counter/:name", (req, res) => {

    if (!counterWithName[req.params.name]) {
        counterWithName[req.params.name] = 1;
    }

    res.send(`<h1>${req.params.name} was here ${counterWithName[req.params.name]} times</h1>`);
    counterWithName[req.params.name]++;
});

app.listen(5000, () => {
    console.log("Listening to port 5000");
});