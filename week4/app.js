import express from "express";
import bcrypt from "bcrypt";
import "dotenv/config";

const app = express();
const PORT = process.env.PORT || 3000;
const correctPassword = await bcrypt.hash("salaSana", 10);

app.use(express.json());

app.post("/login", async (req, res) => {
    const access = await bcrypt.compare(req.body.password, correctPassword);
    if (access) console.log("Logged in");
    else console.log("Wrong password");
    res.end();
});

app.listen(PORT, () => {
    console.log(`Listening to ${PORT}.`);
});

async function checkPassword(password, hash) {
    return await bcrypt.compare(password, hash);
}
