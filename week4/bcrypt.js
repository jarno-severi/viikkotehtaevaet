import bcrypt from "bcrypt";
import "dotenv/config";

const input = process.argv[2];

const password = process.env.CORRECT_PASSWORD;
const correctPassword = await bcrypt.hash(password, 10);
console.log(correctPassword);

async function checkPassword(password, hash) {
    return await bcrypt.compare(password, hash);
}

const access = await checkPassword(input, correctPassword);

if (access) console.log("Logged in");
else console.log("Wrong password");