import express from "express";
// import fs from "fs";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

let id = 0;
let students = [];

app.get("/student/:id", (req, res) => {
    const student = students.find(student => student.id === parseInt(req.params.id));
    res.send(student);
});

app.post("/student", (req, res) => {
    req.body.id = id;
    students.push(req.body);
    id++;
    res.redirect("/");
});

app.put("/student/:id", (req, res) => {
    const student = students.find(obj => obj.id === parseInt(req.params.id));
    console.log(Object.entries(req.body));

    for (const [key, value] of Object.entries(req.body)) {
        student[key] = value;
    }
    
    students[req.params.id] = student;

    /* const newStudent = {...req.body, id: req.params.id};
    students = students.map(student => 
        student.id === req.params.id ? newStudent : student
    ); */

    res.redirect("/");

});

app.delete("/student/:id", (req, res) => {
    students = students.filter(student => student.id !== parseInt(req.params.id));
    res.redirect("/");
});

app.listen(5000, () => {
    console.log("Listening to port 5000");
});