import bcrypt from "bcrypt";
import express from "express";
import "dotenv/config";
import jwt from "jsonwebtoken";

const app = express();

app.use(express.json());

app.post("/login", async (req, res) => {
    /** Get correct password from .env */
    const hash = process.env.CORRECT_PASSWORD;

    /** Get user input to variables */
    const password = req.body.password;
    const username = req.body.username;

    /**  Compare user given password with correct password for access */
    const access = await bcrypt.compare(password, hash);
    
    if (access) {
        const token = jwt.sign(
            username, 
            process.env.TOKEN_SECRET
        );
        res.status(200).send({ token });

    } else {
        res.status(401).send("Wrong password");
    }
});

/** Create router for /getSecrets */
const router = express.Router();
app.use("/getSecrets", router);

/** Use middleware to athenticate token */
router.use((req, res, next) => {
    const token = req.get("authorization");
    
    if (token) {
        const secret = process.env.TOKEN_SECRET;
        const access = jwt.verify(token, secret);

        if (access) next();
        else return res.status(401).end();

    } else return res.status(401).end();

    
});

/** Request made to /getSecrets */
router.post("/", (req, res) => {
    console.log("You did it!");
    res.end();
});


app.listen(5000);