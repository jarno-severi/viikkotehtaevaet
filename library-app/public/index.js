/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

const createUserButton = document.querySelector("#createUserButton");
createUserButton.addEventListener("click", createUser);
const loginButton = document.querySelector("#loginButton");
loginButton.addEventListener("click", loginUser);

let token = null; // Replace with sessionStorage at loginUser()

function createButton(text, id) {
    const button = document.createElement("button");
    button.textContent = text;
    button.id = id;
    return button;
}

function createContent(target) {
    /** Change target style */
    target.style.display = "flex";
    target.style.justifyContent = "center";

    /** Buttons */
    const buttons = [
        createButton("Add book", "postBookButton"), 
        createButton("Get my books", "getBooksButton"), 
        createButton("Edit book", "putBookButton"), 
        createButton("Delete book", "deleteBookButton")
    ];
    
    buttons.forEach(button => target.appendChild(button));
}

function hideContent(target) {
    target.style.display = "none";
}

function loginUser() {
    const username = document.querySelector("#username");
    const password = document.querySelector("#password");

    if (username.value && password.value) {
        const url = "//localhost:5000/login";
        axios.post(url, { username: username.value, password: password.value })
            .then(function (res) {
                alert(`Login success`);
                hideContent(document.querySelector("#login-container"));
                token = res.data; // sessionStorage.setItem("token", res.data);
                createContent(document.querySelector("#user-container"));
            })
            .catch(function (err) {
                console.log(err);
            });
    } else {
        alert("Give username and password");
    }

    username.value = "";
    password.value = "";

}

function createUser() {
    const username = document.querySelector("#username");
    const password = document.querySelector("#password");

    if (username.value.length < 4 || password.value.length < 4) {
        alert("Give username and password length of at least 4");
    } else {
        createUserButton.disabled = true;
        const url = "//localhost:5000/register";
        axios.post(url, { username: username.value, password: password.value })
            .then(function (res) {
                alert(`User ${res.data.username} created`);
            })
            .catch(function (err) {
                console.log(err);
            });

    }

    username.value = "";
    password.value = "";
}

function testFunction() {
    console.log("Hello");
}

