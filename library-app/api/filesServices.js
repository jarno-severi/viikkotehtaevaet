import fs from "fs";

/**
 * Writes books data to JSON file that simulates database
 * @param {string} file -Destination .json file
 * @param {object} data 
 */
export function writeToJSON(file, data) {
    fs.writeFileSync(file, JSON.stringify(data));
}

/**
 * Reads given JSON file
 * @param {string} file 
 * @returns {array} Array of previously saved books or empty
 */
export function readJSON(file) {
    try {
        return JSON.parse(fs.readFileSync(file, "utf-8"));
    } catch {
        return [];
    }
}