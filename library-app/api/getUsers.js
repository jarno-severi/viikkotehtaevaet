import { readJSON } from "./filesServices.js";

export const getUsers = (req, res) => {
    const users = readJSON("users.json");
    const usernames = users.map(user => user.username);
    res.send(usernames);
};