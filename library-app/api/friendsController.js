import { readJSON } from "./filesServices.js";
import { writeToJSON } from "./filesServices.js";

export const getFriends = (req, res) => {
    const users = readJSON("users.json");
    const user = users.find(user => user.id === req.user.id);
    res.send(user.friends);
};

export const postFriend = (req, res) => {
    const users = readJSON("users.json");
    const user = users.find(user => user.id === req.user.id);
    const friend = users.find(user => user.username === req.body.friend);
    const alreayFriend = user.friends.includes(friend.id);

    if (alreayFriend) {
        res.status(400).send("Already in your friends");
    } else if (user && friend) {
        user.friends.push(friend.id);
        writeToJSON("users.json", users);
        res.send(`${friend.username} added`);
    } else {
        res.status(404).send("User not found");
    }
};

export const getFriendsBooks = (req, res) => {
    const users = readJSON("users.json");
    const books = readJSON("books.json");
    
    if (req.params.username) {
        const friend = users.find(user => user.username === req.params.username);

        if (friend) {
            const friendBooks = books.filter(book => book.owner === friend.id);
            res.send(friendBooks);
        } else {
            res.status(404).send("Friend not found");
        }
        
    } else {
        const user = users.find(user => user.id === req.user.id);
        const userFriends = user.friends;
        const friendsBooks = [];

        userFriends.forEach(id => {
            const filteredBooks = books.filter(book => book.owner === id);
            filteredBooks.forEach(book => friendsBooks.push(book));
        });
        
        res.send(friendsBooks);
    }
};