import express from "express";
import jwt from "jsonwebtoken";
import { registerUser } from "./registerUser.js";
import { loginUser } from "./loginUser.js";
import * as booksController from "./booksController.js";
import { getUsers } from "./getUsers.js";
import * as friendsController from "./friendsController.js";

const router = express.Router();

/** 
 * Routes available for every user 
 */
router.post("/register", registerUser);
router.post("/login", loginUser);

/** 
 * Middleware to autheticate user 
 */
router.use((req, res, next) => {
    const auth = req.get("authorization");

    if (auth && auth.startsWith("Bearer ")) {
        try {
            const token = auth.substring(7);
            req.user = jwt.verify(token, process.env.SECRET);
            next();
        } catch (err) {
            res.status(401).send("Unauthorized request");
        }
    } else {
        res.status(401).send("Unauthorized request");
    }
});

/** 
 * Routes for authenticated user:
 * Books
 */
router.get("/books", booksController.getBooks);
router.post("/books", booksController.postBook);
router.get("/books/:id", booksController.getBook);
router.put("/books/:id", booksController.putBook);
router.delete("/books/:id", booksController.deleteBook);

/** Friends and friendbooks */
router.get("/friends", friendsController.getFriends);
router.post("/friends", friendsController.postFriend);
router.get("/friendbooks", friendsController.getFriendsBooks);
router.get(`/friendbooks/:username`, friendsController.getFriendsBooks);

/** Users */
router.get("/users", getUsers);

export default router;