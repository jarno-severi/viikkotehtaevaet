import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import "dotenv/config";
import { readJSON } from "./filesServices.js";


export const loginUser = async (req, res) => {
    /**
     * Create token, if user exists and password is correct
     * @return Token or Unauthorized request
     */
    const users = readJSON("users.json");
    const user = users.find(user => req.body.username === user.username);

    if (user) {
        bcrypt.compare(req.body.password, user.password, (err, access) => {
            if (access) {
                const token = jwt.sign(
                    { id: user.id },
                    process.env.SECRET
                );
                res.send(token);
            } else {
                res.status(401).send("Invalid password");
            }
        });
    } else {
        res.status(404).send("User not found");
    }
};