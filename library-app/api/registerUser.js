import shortid from "shortid";
import bcrypt from "bcrypt";
import { readJSON } from "./filesServices.js";
import { writeToJSON } from "./filesServices.js";

export const registerUser = async (req, res) => {
    const users = readJSON("users.json");

    /** 
     * Check that username and password is given in request
     * @param {string} username
     * @param {string} password
     * @return Username or Bad Request with message
     * */
    if(req.body.username && req.body.password) {
        const cyptedPassword = await bcrypt.hash(req.body.password, 10);
        const randomId = shortid.generate();
        const user = { 
            id: randomId, 
            password: cyptedPassword, 
            username: req.body.username, 
            friends: []
        };
        users.push(user);
        writeToJSON("users.json", users);
        res.send(user.username);
    } else {
        res.status(400).send("Provide username and password");
    }
};