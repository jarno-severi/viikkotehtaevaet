import shortid from "shortid";
import { writeToJSON, readJSON } from "./filesServices.js";

/** 
 * This creates a new book from user request object
 * Crates unique id for the book
 * Adds this book to @const {array} books
 * @returns {object} Created book
 */
export const postBook = (req, res) => {
    const books = readJSON("books.json");
    const newBook = req.body;
    newBook.id = shortid.generate();
    newBook.owner = req.user.id;
    books.push(newBook);
    writeToJSON("books.json", books);
    res.status(201).send(newBook);
};

/** 
 * Returns all the books
 * @returns {array}
 */
export const getBooks = (req, res) => {
    const books = readJSON("books.json");
    const userBooks = books.filter(book => book.owner === req.user.id);
    res.send(userBooks);
};

/** 
 * Finds user requested book by request id
 * @returns {object} Book or error status
 */
export const getBook = (req, res) => {
    const books = readJSON("books.json");
    const book = books.find(book => book.id === req.params.id);

    if (book) {
        res.send(book);
    } else {
        res.status(404).send("Book not found");
    } 
};

/** 
 * Modifies a book with request object
 * @returns {object} Book with modified data
 */
export const putBook = (req, res) => {
    const books = readJSON("books.json");
    const book = books.find(book => book.id === req.params.id);

    if (book) {
        const updateData = Object.entries(req.body);

        for (const [key, value] of updateData) {
            book[key] = value;
        }

        const index = books.findIndex(book => book.id === req.params.id);
        books[index] = book;
        writeToJSON("books.json", books);
        res.status(201).send(book);
    } else {
        res.status(404).send("Book not found");
    }
};

/** 
 * Delete book from books
 * @const {number} index
 * @const {array} books
 * @returns {string} Success message or if not found error status
 */
export const deleteBook = (req, res) => {
    const books = readJSON("books.json");
    const book = books.find(book => book.id === req.params.id);
    const index = books.findIndex(book => book.id === req.params.id);

    if (book) {
        books.splice(index, 1);
        writeToJSON("books.json", books);
        res.send(`Book with id: ${book.id} was deleted.`);
    } else {
        res.status(404).send("Book not found");
    }
};