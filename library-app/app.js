import express from "express";
import router from "./api/router.js";

const app = express();
const PORT = process.env.PORT || 5000;

/** When GET request hits the server, serve static folder */
app.use(express.static("public"));

/** JSON parser for all incoming requests */
app.use(express.json());

/** Router */
app.use("/", router);

/**
 * Listen to given port for requests
 * @param {number} PORT
 */
app.listen(PORT, () => {
    console.log(`Listening to ${PORT}.`);
});