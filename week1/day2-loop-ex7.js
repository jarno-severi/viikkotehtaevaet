let n = 100;
let sum = 0;

for (let i = 1; i <= n; i++) {
    console.log(i);
    if(!(i%3) && !(i%5)) console.log("FizzBuzz");
    else if(!(i%3)) console.log("Fizz");
    else if(!(i%5)) console.log("Buzz");
}

console.log(sum);