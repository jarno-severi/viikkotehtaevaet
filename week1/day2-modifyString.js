const action = process.argv[2];
const inputString = process.argv[3];

if (action === "lower") {
    console.log(inputString.toLowerCase());
} else if (action === "upper") {
    console.log(inputString.toUpperCase());
} else {
    console.log("ERROR");
}