const readline = require("readline");

const rl = readline.createInterface(
    {
        input: process.stdin,
        output: process.stdout
    }
);

rl.question("Kuinka mones kävijä tänään?", (input) => {
    if (input % 2000 === 0) {
        console.log("Saa lahjakortin!!!");
    } else if (input >= 1000 && input % 25 === 0) {
        console.log("Saa ilmapallon!");
    } else {
        console.log("Ei saa mitään. :(");
    }
    rl.close();
});