// Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.
const string = process.argv[4];
console.log(string.replaceAll(process.argv[2], process.argv[3]));

/* // BUUTTI RATKAISU
const toReplace = process.argv[2];
const replaceChar = process.argv[3];
const inputString = process.argv[4];

const regex = new RegExp(toReplace, "g");
const outputString = inputString.replace(regex, replaceChar);

console.log(outputString); */