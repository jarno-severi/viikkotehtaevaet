const languageChoice = process.argv[2];

switch (languageChoice) {
    case "fi":
        console.log("Hei maailma!");
        break;
    case "en":
        console.log("Hello world!");
        break;
    case "es":
        console.log("Hola mundo!");
        break;
    default:
        console.log("Available arguments: 'fi', 'en', 'es'");
        break;
}