// Take all arguments and take first 2 elements out to make names array
const args = process.argv;
const names = args.slice(2);
// Empty array to push inits into
const inits = []; 
// Loop through names to push inits to empty array
names.forEach(name => {
    let init = name[0];
    inits.push(init);
});

console.log(inits.join("."));