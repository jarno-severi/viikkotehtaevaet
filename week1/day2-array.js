const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
// const sorted = arr.sort();
// const removeFirst = arr.slice(1);

/* // Print out every item
console.log(arr.join(" "));

// Print out every item that contains 'r"
const rArray = [];
arr.forEach(el => {
    if(el.includes("r")) rArray.push(el);
});
console.log(rArray.join(" ")); */

/* // Sort the array in alphabetical order
const sortedArr = arr.slice(); // 
sortedArr.sort();
console.log(sortedArr.join(" "));

// Remove first item in the array
const newArray = sortedArr.slice();
newArray.shift();
console.log(newArray.join(" ")); */

// Add 'sipuli' to array
// const addedArr = arr.concat("sipuli");

const addedArr = arr.slice();
addedArr.push("sipuli"); 
// addedArr[addedArr.length] = "sipuli"; 

console.log(addedArr.join(" ")); 

/* CLONE ARRAY WITH SPREAD OPERATOR [...arr] */