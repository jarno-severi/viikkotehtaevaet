/* // 1. and 2.
for(let i = 0; i <= 1000; i += 100) {
    console.log(i);
}

for(let i = 1; i <= 128; i *= 2) {
    console.log(i);
} */

/* // 3. and 4.
for(let i = 3; i <= 15; i += 3) {
    console.log(i);
}

for(let i = 9; i >= 0; i--) {
    console.log(i);
} */

/* // 5. 111 222 333 444
for (let i = 1; i < 5; i++) {
    for (let j = 0; j < 3; j++) {
        console.log(i);
    }
} */

/* // 6. 01234 01234 01234 01234
for(let i = 0; i < 3; i++) {
    for(let j = 0; j < 5; j++) {
        console.log(j);
    }
} */

// Exercise 5: 1 to n, n = number
function sumOfInts() {
    let number = 10;
    let sum = 0;

    // for loop
    // for (let i = 1; i <= number; i++) {
    //     sum = sum + i;
    // }
    
    // while loop
    let ind = 0;
    while (ind <= number) {
        sum = sum + ind;
        ind++;
    }

    return console.log(sum);
}

sumOfInts();