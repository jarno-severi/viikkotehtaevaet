const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

// Find largest
function findLargestNumber(arr) {
    let max = -Infinity;
    for(let i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
}

// Find second largest
function findSecodLargest(arr) {
    const largestNumber = findLargestNumber(arr);
    let secondLargest = -Infinity;
    for(let i = 0; i < arr.length; i++) {
        if (arr[i] > secondLargest && arr[i] < largestNumber) {
            secondLargest = arr[i];
        }
    }
    return secondLargest;
}

console.log(findLargestNumber(arr)); 
console.log(findSecodLargest(arr));