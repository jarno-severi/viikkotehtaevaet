const students = [
    { name: "markku", score: 99 }, { name: "karoliina", score: 58 },
    { name: "susanna", score: 69 }, { name: "benjamin", score: 77 },
    { name: "isak", score: 49 }, { name: "liisa", score: 89 },
];

// Find the highest, lowest scoring person.
function findLargestScore(arr) {
    let max = -Infinity;
    let person;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].score > max) {
            max = arr[i].score;
            person = arr[i];
        }
    }

    return person;
}

function findSmallestScore(arr) {
    let min = Infinity;
    let person;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].score < min) {
            min = arr[i].score;
            person = arr[i];
        }
    }
    return person;
}

console.log(findLargestScore(students));
console.log(findSmallestScore(students));

// Find the average score of the students
function findAvarage(arr) {

    // Sum all scores
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i].score;
    }

    // Divide sum by students
    const studentAmount = arr.length;
    const avarage = sum / studentAmount;

    return avarage;
}

// Print out only the students who scored higher than the average
let avarageScore = findAvarage(students);
students.forEach(student => {
    if (student.score > avarageScore) console.log(`${student.name} scored higher than avarage`);
    if (student.score > 94) student.grade = "5";
    else if (student.score > 79 && student.score < 95) student.grade = "4";
    else if (student.score > 59 && student.score < 80) student.grade = "3";
    else if (student.score > 39 && student.score < 60) student.grade = "2";
    else if (student.score > 0 && student.score < 40) student.grade = "1";
});

console.table(students);