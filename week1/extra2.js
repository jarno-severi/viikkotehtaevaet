const readline = require("readline");
const rl = readline.createInterface(
    {
        input: process.stdin,
        output: process.stdout
    }
);

// Functions
function rollDice(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return `The roll is ${Math.floor(Math.random() * (max - min + 1) + min)}`;
}

// Check for valid input and that number given is between 3 and 100.
const inputArgument = process.argv[2];
const argsTrue = process.argv.length > 2;

if (argsTrue) {
    const inputLength = inputArgument.length > 1;
    const firstIsD = inputArgument[0] === "D";
    const parseNumber = parseInt(inputArgument.substring(1));
    const isNumber = !(isNaN(parseNumber));
    const validNumber = parseNumber > 2 && parseNumber < 101;
    let dice = parseNumber;

    if (inputLength && firstIsD && isNumber && validNumber) {
        console.log(`Press <enter> for a dice roll, type 'q' + <enter> to exit`);

    } else {
        // Invalid input
        if (!firstIsD || !isNumber) {
            console.log("Argument must be 'DN', where N is number of sides.");
            rl.close();
        }
        else if (!validNumber) {
            // Fallback value if number is lower than 3
            console.log("Falling back to default of 6 sides 'D6'");
            dice = 6;
        }
    }

    // Input OK, give command to quit program or to roll dice
    rl.on("line", (input) => {
        if (input === "q") {
            rl.close(); // END
        } else {
            console.log(rollDice(1, dice));
            console.log(`Press <enter> for a dice roll, type 'q' + <enter> to exit`);
        }
    });

// No arguments
} else {
    console.log(`ERROR: Command line argument not found. Argument must be 'DN', where N is number of sides.`);
    rl.close();
}