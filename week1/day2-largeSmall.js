// Take arguments from user input
const args = (process.argv).slice(2);

// Convert array items to integer
const array = [];
args.forEach(element => {
    array.push(parseInt(element));
});

// Find largest
function findLargestNumber(arr) {
    let max = -Infinity;
    for(let i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return console.log(`Largest: ${max}`);
}

// Find smallest
function findSmallestNumber(arr) {
    let min = Infinity;
    for(let i = 0; i < arr.length; i++) {
        if (arr[i] < min) {
            min = arr[i];
        }
    }
    return console.log(`Smallest: ${min}`);
}

// All equal
function isAllEqual(arr) {
    for(let i = 0; i < arr.length; i++) {
        if (arr[i] === arr[i+1]) {
            return console.log("All equal");
        } else {
            return false;
        }
    }
}

findLargestNumber(array);
findSmallestNumber(array);
isAllEqual(array);