const checkBalance = true;
const balance = 110;
const isActive = true;

if (checkBalance === false) console.log("Have a nice day!");
else if (isActive === false) console.log("Your account is not active");
else if (balance === 0) console.log("Your account is empty");
else if (balance < 0) console.log("Your balance is negative!");
else console.log(`Account balance: ${balance}`);