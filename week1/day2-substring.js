const inputString = process.argv[2];
const stringToArray = inputString.split(" ");
const annoyingString = stringToArray.slice(0, -1);

console.log("Original string: " + stringToArray.join(" "));
console.log("Annoying string: " + annoyingString.join(" "));