/* // Excercise 2
// The process.argv property is an inbuilt application programming interface of the process module which is used to get the arguments passed to the node.js process when run in the command line. [0] = node.exe, [1] = argv
const a = parseFloat(process.argv[2]);
const b = parseFloat(process.argv[3]);
console.log(`type of a: ${typeof a}, value of a: ${a} and b: ${b}`);

const sum = a + b;
const difference = a - b;
const fraction = a / b;
const product = a * b;
const exponent = a ** b;
const modulo = a % b;
const avarage = (a + b) / 2

console.log(`sum: ${sum}`);
console.log(`difference: ${difference}`);
console.log(`fraction: ${fraction}`);
console.log(`product: ${product}`);
console.log(`exponent: ${exponent}`);
console.log(`modulo: ${modulo}`); 
console.log(`avarage: ${avarage}`); */

/* // Excercise 3
const str1 = "fiz";
const str2 = "buzz";
const str_sum = str1 + str2;
const str_avg = (str1.length + str2.length) / 2;
console.log(`Avarage: ${str_avg}`);

if (str1.length < str_avg) {
    console.log(str1.length);
}

if(str2.length < str_avg) {
    console.log(str2.length);
}

if (str_sum < str_avg) {
    console.log(str_sum);
} */

/* // Excercise 4 
const playerCount = 4;
if (playerCount === 4) {
    console.log("Play the game!");
}

let isStressed = true;
let hasIceCream = false;
if(!isStressed || hasIceCream){
    console.log("Mark is happy");
}

let isShining = true;
let isRaining = false;
let temperature = 19;
if (isShining && !isRaining && temperature >= 20) {
    
    console.log("It's a beach day");
} */

/* // Exercise 5: Arin is happy if he sees either Suzy or Dan on Tuesday night. However, seeing them both at the same time, or being alone, makes him sad.
const seesSuzy = true;
const seesDan = false;
const isTuesday = true;

if ((seesSuzy !== seesDan) && isTuesday) {
    console.log("Arin is happy");
} else {
    console.log("Arin is sad");
} */

/* // Exercise 6
// String methods
const input = process.argv[2];
const trimInput = input.trim();
const maxLength = trimInput.substring(0, 20);
const firstLetter = maxLength[0].toLowerCase();
const string = maxLength.slice(1);

// Conditions
const noWhiteSpace = input === trimInput;
const lessThanMaxLength = trimInput.length < 20;
const noCapitalLetter = maxLength[0] === firstLetter;
if (noWhiteSpace && lessThanMaxLength && noCapitalLetter) {
    const output = firstLetter + string;
    console.log(output);
} else {
    console.log("Error");
} */

console.log("Hello world!");