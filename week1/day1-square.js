const input = process.argv[2];
const side = parseFloat(input);
if(side || side === 0) {
    const area = side * side;
    console.log(area);
} else {
    console.log("Please give a number");
}