/* // LEVEL 1
let height = 4;
let string = "";
for(let i = 0; i < height; i++) {
    string += "&";
    console.log(string);
} */

/* // LEVEL 2
let height = 4;
// console.log("alkaa");
for (let i = 0; i < height; i++) {
    // console.log("tullaan ensimmaiseen looppiin");
    // console.log(`${height} / ${i}`);
    const symbolAmount = 2 * i + 1;
    let string = "";

    for (let j = 1; j < height - i; j++) {
        // console.log("tullaan valilyonti looppiin");
        string += " ";
    }

    for (let j = 0; j < symbolAmount; j++) {
        // console.log("tullaan symboli looppiin");
        // console.log(`${symbolAmount} / ${j}`);
        string += "&";
    }
    console.log(string);
} */

// LEVEL 3
let height = 4;
let iterator = 0;

while (iterator < height) {
    const symbolAmount = 2 * iterator + 1;
    let string = "";
    
    let spaceIterator = 1;
    while (spaceIterator < height - iterator) {
        string += " ";
        spaceIterator++;
    }
    
    let symbolIterator = 0;
    while (symbolIterator < symbolAmount) {
        string += "&";
        symbolIterator++;
    }

    console.log(string);
    iterator++;
}